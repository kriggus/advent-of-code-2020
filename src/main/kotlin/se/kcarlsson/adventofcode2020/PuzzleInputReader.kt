package se.kcarlsson.adventofcode2020

import java.io.File

class PuzzleInputReader {

    fun read(): List<String> {
        val filePath = javaClass.classLoader.getResource("puzzleinput.txt")?.file ?: return emptyList()
        val file = File(filePath)
        return file.readLines()
    }
}