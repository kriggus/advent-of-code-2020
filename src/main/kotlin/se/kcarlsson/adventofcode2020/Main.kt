package se.kcarlsson.adventofcode2020


fun main(args: Array<String>) {
    val puzzleInputs = PuzzleInputReader().read()
    val solution = PuzzleSolver().solve(puzzleInputs)
    println("Solution: $solution")
}